import asyncio
from mpikat.core.edd_pipeline_aio import EDDPipeline, launchPipelineServer, state_change


class HelloEDD(EDDPipeline):
    pass



def main():
    """The main function wrapper
    """
    asyncio.run(launchPipelineServer(HelloEDD))

if __name__ == "__main__":
    main()

