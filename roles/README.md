# Roles
To provide services for the EDD backend, we define ansible roles. If you are not familiar with ansible take a quick look [here](https://gitlab.mpcdf.mpg.de/mpifr-bdg/edd_provisioning#ansible-basics).
A role (or the service) is defined in a sub folder (e.g. roles/hello_edd).
