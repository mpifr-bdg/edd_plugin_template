# edd_plugin_template

This repository is a template repository for developing EDD backend plugins. It
defines one pipeline `hello_edd`

## Directory structure

./roles - ansible roles defined by this plugin
./test
./eddplugin
./galaxy.yml  - Ansible galaxy requirements
./requirements.yml  - Ansible galaxy requirements


## Usage of this template

1) Copy all files from this repository into a new folder 
2) Change files to match name of plugin
  - $ mv eddplugin MY_PLUGIN
  - $ mv roles/hello_edd roles/MY_PIPELINE
  - edit py_project.toml to install MYPIPELINE
  - modify `roles/MY_PIPELINE/templates/Dockerfile` to correctly build pipeline 
  - modify `roles/MY_PIPELINE/tasks/main.yml` to contain correct mpikat pipeline
    script and image name





